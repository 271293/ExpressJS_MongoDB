var tab=[];
var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
app.use('/javascript', express.static(__dirname + '/javascript'));
app.use('/lib', express.static(__dirname + '/lib'));
MongoClient.connect("mongodb://localhost:27017/maDB", function(err, db) {
    if(!err) {
        console.log("Connexion à maDB : OK");
    }
    db.collection('fiches').find().toArray(function(err, items) {
        tab=items;
    });
});
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/html/index.html');
});
app.get('/api/affiche/', function(req, res) {
    res.json(tab);
});
app.listen(8080);